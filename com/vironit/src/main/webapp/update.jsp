<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 11/2/2018
  Time: 5:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update</title>
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
    <form action="update" method="post">
        <input name="id" type="number" required value="${good.getId()}" style="visibility: hidden">
        <select name="brand" name="field1">
            <option value="Xiaomi">Xiaomi</option>
            <option value="Apple">Apple</option>
            <option value="Lenovo">Lenovo</option>
        </select>
        <input name="model" type="text" required value="${good.getModel()}" name="field2">
        <select name="category">
            <option value="Phone">Phone</option>
            <option value="Case">Case</option>
            <option value="Wire">Wire</option>
            <option value="Headphone">Headphone</option>
            <option value="Battery">Battery</option>
        </select>
        <input name="description" type="text" name="field3" value="${good.getDescription()}">
        <input name="price" type="number" required min="1" max="100000" value="${good.getPrice()}" name="field4">
        <input name="quantity" type="number" required min="1" max="100" value="${good.getQuantity()}" name="field5">
        <input type="submit" value="Submit">
    </form>
</div>
</body>
</html>
