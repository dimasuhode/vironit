<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="model.UserEntity" %>
<%@ page import="org.springframework.security.core.context.SecurityContext" %>
<%@ page import="javax.xml.ws.Action" %>
<%@ page import="org.springframework.beans.factory.annotation.Autowired" %><%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/8/2018
  Time: 11:31 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Index</title>
    <link href="css/add.css" rel="stylesheet">
</head>
<body>

<sec:authorize access="isAuthenticated()">
    <div class="form-style-6">
        Hello <sec:authentication property="principal.username"/>
    </div>
</sec:authorize>
<sec:authorize access="!isAuthenticated()">
    <%--<sec:authorize access="hasRole()">--%>
    <%--</sec:authorize>--%>
    <div class="form-style-6">Hello Nameless One</div>
</sec:authorize>
<sec:authorize access="hasRole(\"ADMIN\")">
    <div class="form-style-6"><a href="alluser">View all user</a></div>
    <div class="form-style-6"><a href="adminlist">Update and Delete good</a></div>
</sec:authorize>
<div class="form-style-6">
    <%--<%--%>
        <%--if (session.getAttribute("error") == null) {--%>
            <%--response.getWriter().append("<div class=\"form-style-6\">Hello Nameless One</div>");--%>
        <%--} else if (((boolean) session.getAttribute("error"))) {--%>
            <%--response.getWriter().append("<div class=\"form-style-6\">Hello Nameless One</div>");--%>
        <%--} else {--%>
            <%--UserEntity user = (UserEntity) session.getAttribute("user");--%>
            <%--response.getWriter().append("<div class=\"form-style-6\">Hello " + user.getLogin() + "</div>");--%>
            <%--if (user.getRole().getValue().equals("Role_Admin")) {--%>
                <%--response.getWriter().append("<div class=\"form-style-6\"><a href=\"alluser\">View all user</a></div>");--%>
                <%--response.getWriter().append("<div class=\"form-style-6\"><a href=\"adminlist\">Update and Delete good</a></div>");--%>
            <%--}--%>
        <%--}--%>
    <%--%>--%>
    <a href="main">View all goods</a>
</div>
<div class="form-style-6">
    <a href="add.jsp">Create good</a>
</div>
<sec:authorize access="!isAuthenticated()">
    <div class="form-style-6">
        <a href="registration.jsp">Registration</a>
    </div>
    <div class="form-style-6">
        <a href="entry.jsp">Entry</a>
    </div>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <div class="form-style-6"><a href="logout">Logout</a></div>"
</sec:authorize>
<div class="form-style-6">
    <a href="basket.jsp">Basket</a>
</div>
</body>
</html>
