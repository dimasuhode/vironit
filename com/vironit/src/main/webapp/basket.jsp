<%@ page import="model.GoodEntity" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/15/2018
  Time: 12:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Basket</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
    <%
//        System.out.println("session = "+session.getAttribute("list"));
        List<GoodEntity> list = (List<GoodEntity>) session.getAttribute("list");
                if(list==null){
                    list=new ArrayList<>();
                };
        double x = list.stream().mapToDouble(GoodEntity::getPrice).sum();
        session.setAttribute("price",x);
    %>
    <table class="simple-little-table" cellspacing='0'>
        <tr>
            <th>Brand</th>
            <th>Model</th>
            <th>Price</th>
        </tr>
        <c:forEach items="${list}" var="good">
            <tr>
                <td><c:out value="${good.getBrand().getValue()}"></c:out></td>
                <td><c:out value="${good.getModel()}"></c:out></td>
                <td><c:out value="${good.getPrice()}"></c:out></td>
            </tr>
        </c:forEach>
        <tr>
            <th>-</th>
            <th>-</th>
            <th><c:out value="${price}"></c:out></th>
        </tr>
    </table>
</div>
<div class="form-style-6">
    <button onclick="window.location='http://localhost:8080/buy'">Buy</button>
    <button onclick="window.location='http://localhost:8080'">Back</button>
</div>
</body>
</html>
