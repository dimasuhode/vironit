<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.GoodEntity" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/8/2018
  Time: 12:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
    <table class="simple-little-table" cellspacing='0'>
        <tr>
            <th>Brand</th>
            <th>Model</th>
            <th>Price</th>
        </tr>
        <c:forEach items="${goods}" var="good">
            <tr>
                <td><c:out value="${good.getBrand().getValue()}"></c:out></td>
                <td><c:out value="${good.getModel()}"></c:out></td>
                <td><c:out value="${good.getPrice()}"></c:out></td>
                <td><a href="/basket?good=${good.id}">Add in Basket</a></td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="form-style-6">
    <button onclick="window.location='http://localhost:8080/main'">Update</button>
    <button onclick="window.location='http://localhost:8080'">Back</button>
</div>
</body>
</html>
