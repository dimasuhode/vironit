<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="model.UserEntity" %><%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/31/2018
  Time: 5:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>AdminPage</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
  <%--<%List<UserEntity> alluser = (List<UserEntity>) request.getAttribute("alluser");%>--%>

  <table class="simple-little-table" cellspacing='0'>
      <tr>
          <th>Login</th>
          <th>Email</th>
          <th>Password</th>
      </tr>
      <c:forEach items="${alluser}" var="user">
          <tr>
              <td><c:out value="${user.getLogin()}"></c:out></td>
              <td><c:out value="${user.getEmail()}"></c:out></td>
              <td><c:out value="${user.getPassword()}"></c:out></td>
          </tr>
      </c:forEach>
  </table>
</div>
<div class="form-style-6">
    <button onclick="window.location='http://localhost:8080'">Back</button>
</div>
</body>
</html>
