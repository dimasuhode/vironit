<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 11/2/2018
  Time: 4:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Update and Delete</title>
    <link href="css/main.css" rel="stylesheet">
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
<table class="simple-little-table" cellspacing='0'>
    <tr>
        <th>Brand</th>
        <th>Model</th>
        <th>Price</th>
        <th></th>
        <th></th>
    </tr>
    <c:forEach items="${goods}" var="good">
        <tr>
            <td><c:out value="${good.getBrand().getValue()}"></c:out></td>
            <td><c:out value="${good.getModel()}"></c:out></td>
            <td><c:out value="${good.getPrice()}"></c:out></td>
            <td><a href="/update?good=${good.id}">Update</a></td>
            <td><a href="/delete?good=${good.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>
<button onclick="window.location='http://localhost:8080'">Back</button>
</div>
</body>
</html>
