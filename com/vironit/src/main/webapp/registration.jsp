<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/11/2018
  Time: 12:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Registration</title>
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<%if (session.getAttribute("reg")!=null && !((boolean)session.getAttribute("reg"))){
    response.getWriter().append("<script>alert(\"Error : Incorrect login or mail \");</script>");
    session.invalidate();
}%>
<div class="form-style-6">
    <form action="registration" method="post">
        <input name="login" type="text" required placeholder="Your login.." name="field1">
        <input name="email" type="email" placeholder="Email.." name="field">
        <input name="password" type="password" required placeholder="Password" class="field2" style="margin-bottom: 4%;padding: 3%;width: 100%;">
        <input type="submit" value="Submit">
    </form>

    <button onclick="window.location='http://localhost:8080'">Back</button>
</div>
</body>
</html>
