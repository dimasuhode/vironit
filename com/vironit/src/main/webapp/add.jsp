<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: dima
  Date: 10/9/2018
  Time: 12:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new model</title>
    <link href="css/add.css" rel="stylesheet">
</head>
<body>
<div class="form-style-6">
    <form action="main" method="post">
        <select name="brand" name="field1">
            <option value="Xiaomi">Xiaomi</option>
            <option value="Apple">Apple</option>
            <option value="Lenovo">Lenovo</option>
        </select>
        <input name="model" type="text" required placeholder="Model name.." name="field2">
        <select name="category">
            <option value="Phone">Phone</option>
            <option value="Case">Case</option>
            <option value="Wire">Wire</option>
            <option value="Headphone">Headphone</option>
            <option value="Battery">Battery</option>
        </select>
        <input name="description" type="text" name="field3">
        <input name="price" type="number" min="1" max="100000" required placeholder="Price.." name="field4">
        <input name="quantity" type="number" min="1" max="100" required placeholder="Quantity.." name="field5">
        <input type="submit" value="Submit">
    </form>

    <button onclick="window.location='http://localhost:8080'">Back</button>
    <% if (request.getAttribute("save") != null) {
        boolean save = (boolean) request.getAttribute("save");
        if (save) {
            response.getWriter().append("<h2 class=\"form-style-6\">Save</h2>");
        } else {
            response.getWriter().append("<h2 class=\"form-style-6\">Error</h2>");
        }
    }%>
</div>
</body>
</html>
