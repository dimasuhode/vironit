package controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.BrandEntity;
import model.CategoriesEntity;
import model.GoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.*;
import service.BrandService;
import service.CategoriesService;
import service.GoodService;

import javax.annotation.security.RolesAllowed;


@RestController
public class GoodController {
    @Autowired
    GoodService goodService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoriesService categoriesService;

    @GetMapping("/good")
    public HttpEntity<String> getAll() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(goodService.getAll());
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/good")
    public HttpEntity<String> createGood(@RequestBody GoodEntity goodEntity) {
        BrandEntity brandEntity = brandService.getByValue(goodEntity.getBrand().getValue());
        CategoriesEntity categoryEntity = categoriesService.getByValue(goodEntity.getCategory().getValue());
        goodEntity.setBrand(brandEntity);
        goodEntity.setCategory(categoryEntity);
        goodService.save(goodEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/good")
    @PreAuthorize("hasRole(\"ADMIN\")")
    public HttpEntity<String> update(@RequestBody GoodEntity goodEntity) {
        goodService.update(goodEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/good")
    @PreAuthorize("hasRole(\"ADMIN\")")
    public HttpEntity<String> deleteGood(@RequestBody GoodEntity goodEntity) {
        goodService.delete(goodEntity);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
