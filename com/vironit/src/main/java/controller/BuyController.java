package controller;

import model.BasketEntity;
import model.GoodEntity;
import model.OrderEntity;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;
import service.OrderService;
import service.UserService;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/buy")
public class BuyController {
    @Autowired
    private HttpSession session;
    @Autowired
    private OrderService orderService;
    @Autowired
    private UserService userService;

    @GetMapping
    public RedirectView doGet() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        UserEntity userEntity = userService.getByLogin(login);
        if (userEntity == null) {
            return new RedirectView("/registration.jsp");
        } else {
            List<GoodEntity> list = (List<GoodEntity>) session.getAttribute("list");
            if (list == null || list.size() < 1) {
                return new RedirectView("/");
            }
            BasketEntity basketEntity = new BasketEntity();
            basketEntity.setGoodId(list);
            OrderEntity orderEntity = new OrderEntity(userEntity, basketEntity, Timestamp.valueOf(LocalDateTime.now()), (double) session.getAttribute("price"));
            orderService.save(orderEntity);
            session.setAttribute("list", null);
            return new RedirectView("/");
        }

    }
}
