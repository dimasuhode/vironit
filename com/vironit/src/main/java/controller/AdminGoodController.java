package controller;

import dao.GoodDaoImpl;
import model.GoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import service.GoodService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminGoodController {
    @Autowired
    private HttpSession session;
    @Autowired
    GoodService goodService;

    @GetMapping("/adminlist")
    public ModelAndView getAllGood() {
        ModelAndView modelAndView = new ModelAndView("updatedelete");
        session.setAttribute("goods", goodService.getAll());
        return modelAndView;
    }
}
