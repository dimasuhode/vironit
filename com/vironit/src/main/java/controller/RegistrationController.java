package controller;

import lombok.extern.log4j.Log4j;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import service.RoleService;
import service.UserService;

import javax.servlet.http.HttpSession;

@Controller
@Log4j
public class RegistrationController {
    @Autowired
    private HttpSession session;
    @Autowired
    private RoleService roleService;
    @Autowired
    private UserService userService;

    @PostMapping("/registration")
    public RedirectView doPost(@RequestParam String login, @RequestParam String password, @RequestParam String email) {
        UserEntity userEntity = new UserEntity(login, email, password, false, roleService.getRoleUser());
        try {
            if (email.matches(".+@.+") && userService.save(userEntity)) {
                return new RedirectView("/");
            } else {
                session.setAttribute("reg", false);
                return new RedirectView("/registration.jsp");
            }
        } catch (Exception e) {
            session.setAttribute("reg", false);
            log.warn("Not Registr " + e.getMessage());
            return new RedirectView("/registration.jsp");
        }
    }
}
