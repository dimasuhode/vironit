package controller;

import dao.UserDaoImpl;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import service.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private HttpSession session;
    @Autowired
    UserService userService;

    @GetMapping("/alluser")
    public ModelAndView getAllUser() {
        ModelAndView modelAndView = new ModelAndView("/adminpage");
        session.setAttribute("alluser", userService.getAll());
        return modelAndView;
    }
}
