package controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import service.UserService;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.DenyAll;
import javax.ejb.Stateless;

@Log4j
@DeclareRoles({"ROLE_ADMIN","ROLE_USER"})
@Stateless
@RestController("/user")
@ExceptionHandler()
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    @DenyAll
    @PreAuthorize("hasRole(\"ADMIN\")")
    public HttpEntity<String> getAllUser() {
        ObjectMapper mapper = new ObjectMapper();
        String result = null;
        try {
            result = mapper.writeValueAsString(userService.getAll());
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @DeleteMapping
    @PreAuthorize("hasRole(\"ADMIN\")")
    public HttpEntity<String> deleteUser(@RequestBody UserEntity id) {
        UserEntity userEntity = userService.getById(id.getId());
        if (userEntity != null) {
            userService.delete(userEntity);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("hasRole(\"ADMIN\")")
    public HttpEntity<String> findUserById(@RequestBody UserEntity id) {
        UserEntity userEntity = userService.getById(id.getId());
        if (userEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        ObjectMapper mapper = new ObjectMapper();
        String json = null;
        try {
            json = mapper.writeValueAsString(userEntity);
        } catch (JsonProcessingException e) {
            log.warn("Error parse object to json " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(json, HttpStatus.OK);
    }
}
