package controller;

import model.GoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;
import service.GoodService;

@Controller
public class DeleteGoodController {
    @Autowired
    GoodService goodService;

    @GetMapping("/delete")
    public RedirectView deleteGood(@RequestParam Long good) {
        GoodEntity entity = goodService.getById(Long.valueOf(good));
        if (entity != null) {
            goodService.delete(entity);
            return new RedirectView("adminlist");
        } else {
            return new RedirectView("adminlist");
        }
    }
}
