package controller;

import lombok.extern.log4j.Log4j;
import model.BasketEntity;
import model.GoodEntity;
import model.OrderEntity;
import model.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import service.GoodService;
import service.OrderService;
import service.UserService;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@Log4j
@RequestMapping("/basket")
public class BasketController {
    @Autowired
    private HttpSession session;
    @Autowired
    private GoodService goodService;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderService orderService;


    @GetMapping
    public HttpEntity<String> addGoodInBasket(@RequestParam(required = false) Long good) {
        if (session.getAttribute("list") == null) {
            List<GoodEntity> list = new ArrayList<>();
            session.setAttribute("list", list);
        }
        List<GoodEntity> list = (List<GoodEntity>) session.getAttribute("list");

        if (good != null && goodService.getById(good) != null) {
            list.add(goodService.getById(Long.valueOf(good)));
            session.setAttribute("list", list);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    public HttpEntity<String> addBasketInOrder() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        UserEntity userEntity = userService.getByLogin(login);
        if (userEntity == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            List<GoodEntity> list = (List<GoodEntity>) session.getAttribute("list");
            if (list == null || list.size() < 1) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            BasketEntity basketEntity = new BasketEntity();
            basketEntity.setGoodId(list);
            OrderEntity orderEntity = new OrderEntity(userEntity, basketEntity, Timestamp.valueOf(LocalDateTime.now()), (double) session.getAttribute("price"));
            orderService.save(orderEntity);
            session.setAttribute("list", null);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @DeleteMapping
    public HttpEntity<String> deleteGoodInBasket(@RequestParam(required = false) Long good) {
        if (session.getAttribute("list") == null || good == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<GoodEntity> list = (List<GoodEntity>) session.getAttribute("list");
        for (GoodEntity goodEntity : list) {
            if (goodEntity.getId() == good) {
                list.remove(goodEntity);
            }
        }
        session.setAttribute("list", list);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
