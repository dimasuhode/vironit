package controller;

import lombok.extern.log4j.Log4j;
import model.BrandEntity;
import model.CategoriesEntity;
import model.GoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import service.BrandService;
import service.CategoriesService;
import service.GoodService;

import javax.servlet.http.HttpSession;

@Log4j
@Controller
@RequestMapping("/update")
public class UpdateGoodController {
    @Autowired
    private HttpSession session;
    @Autowired
    GoodService goodService;
    @Autowired
    BrandService brandService;
    @Autowired
    CategoriesService categoriesService;

    @GetMapping
    public RedirectView getUpdateJSP(@RequestParam Long good) {
        RedirectView redirectView = new RedirectView("/update.jsp");
        try {
            if (good != null && goodService.getById(Long.valueOf(good)) != null) {
                GoodEntity entity = goodService.getById(Long.valueOf(good));
                session.setAttribute("good", entity);
                return redirectView;
            } else {
                redirectView = new RedirectView("/adminlist");
                return redirectView;
            }
        } catch (Exception e) {
            log.warn("Error parse to long: " + e.getMessage());
            redirectView = new RedirectView("/adminlist");
            return redirectView;
        }

    }

    @PostMapping
    public RedirectView updateGood(@ModelAttribute GoodEntity goodEntity) {

        RedirectView redirectView = new RedirectView("/adminlist");
        BrandEntity brandEntity = brandService.getByValue(goodEntity.getBrand().getValue());
        CategoriesEntity categoryEntity = categoriesService.getByValue(goodEntity.getCategory().getValue());
        goodEntity.setCategory(categoryEntity);
        goodEntity.setBrand(brandEntity);
        goodService.update(goodEntity);
        return redirectView;
    }
}
