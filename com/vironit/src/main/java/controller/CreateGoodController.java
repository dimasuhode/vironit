package controller;

import lombok.extern.log4j.Log4j;
import model.BrandEntity;
import model.CategoriesEntity;
import model.GoodEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import service.BrandService;
import service.CategoriesService;
import service.GoodService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Log4j
@Controller
@RequestMapping("/main")
public class CreateGoodController {
    @Autowired
    private HttpSession session;
    @Autowired
    private GoodService goodService;
    @Autowired
    private BrandService brandService;
    @Autowired
    private CategoriesService categoriesService;

    @GetMapping
    public RedirectView getAllGood() {
        RedirectView redirectView = new RedirectView("/main.jsp");
        List<GoodEntity> goodEntities = goodService.getAll();
        session.setAttribute("goods", goodEntities);
        return redirectView;
    }

    @PostMapping
    public RedirectView createGood(@ModelAttribute GoodEntity goodEntity) {
        RedirectView redirectView = new RedirectView("/add.jsp");
        BrandEntity brandEntity = brandService.getByValue(goodEntity.getBrand().getValue());
        CategoriesEntity categoryEntity = categoriesService.getByValue(goodEntity.getCategory().getValue());
        goodEntity.setBrand(brandEntity);
        goodEntity.setCategory(categoryEntity);
        goodService.save(goodEntity);
        return redirectView;
    }
}
