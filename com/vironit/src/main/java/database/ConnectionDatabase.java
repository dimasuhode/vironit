package database;

import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

@Log4j
public class ConnectionDatabase {
    private static ResourceBundle databaseBundle = ResourceBundle.getBundle("database");
    private static String URL= databaseBundle.getString("url");
    private static final String USER = databaseBundle.getString("user");
    private static final String PASS = databaseBundle.getString("password");
    private static Connection connection=null;

    private ConnectionDatabase() {

    }
    public static Connection getConnection(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
           log.warn("JDBC driver not found "+e.getMessage());
        }
        if(connection==null) {
            try {
                connection = DriverManager.getConnection(URL, USER, PASS);
            } catch (SQLException e) {
                log.warn("Connection not create"+e.getMessage());
            }
            return connection;
        }else {
            return connection;
        }
        }


    public static String getURL() {
        return URL;
    }
}
