package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;


import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Data
@Table(name = "goods",schema = "public")
@Entity
public class GoodEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brand")
    private BrandEntity brand;
    @Column(name = "model")
    private String model;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category")
    private CategoriesEntity category;
    @Column(name = "quantity")
    private long quantity;
    @Column(name = "price")
    private double price;
    @Column(name = "description")
    private String description;
    @JsonIgnore
    @ManyToMany(mappedBy = "goodId")
    private List<BasketEntity> basketEntityList = new ArrayList<>();

    public GoodEntity() {
    }

    public GoodEntity(BrandEntity brand, String model, CategoriesEntity category, long quantity, double price, String description) {
        this.brand = brand;
        this.model = model;
        this.category = category;
        this.quantity = quantity;
        this.price = price;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoodEntity that = (GoodEntity) o;
        return id == that.id && quantity == that.quantity && Double.compare(that.price, price) == 0 && Objects.equals(brand, that.brand) && Objects.equals(model, that.model) && Objects.equals(category, that.category) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, brand, model, category, quantity, price, description);
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", brand=" + brand + ", model='" + model + '\'' + ", category=" + category + ", quantity=" + quantity + ", price=" + price + ", description='" + description + '\'' + '}';
    }
}
