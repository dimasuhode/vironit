package model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "users",schema = "public")
public class UserEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "login")
    private String login;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "active")
    private boolean active;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role")
    private RoleEntity role;

    public UserEntity() {
    }

    public UserEntity(String login, String email, String password, boolean active, RoleEntity role) {
        this.login = login;
        this.email = email;
        this.password = password;
        this.active = active;
        this.role = role;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", login='" + login + '\'' + ", email='" + email + '\'' + ", password='" + password + '\'' + ", active=" + active + ", role=" + role + '}';
    }
}
