package model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "basket", schema = "public" , catalog = "vironIT")
@Entity
public class BasketEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToMany(cascade = {CascadeType.MERGE,CascadeType.ALL})
    @JoinTable(name = "basket_goods", joinColumns = { @JoinColumn(name = "basket_id") }, inverseJoinColumns = { @JoinColumn(name = "good_id") })
    private List<GoodEntity> goodId;

    public BasketEntity() {

    }

    public BasketEntity(List<GoodEntity> goodId) {

        this.goodId = goodId;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", goodId=" + goodId + '}';
    }
}
