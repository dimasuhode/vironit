package model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "orders", schema = "public")
public class OrderEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private UserEntity userId;
    @ManyToOne(fetch = FetchType.EAGER,cascade = {CascadeType.ALL})
    @JoinColumn(name = "basket_id")
    private BasketEntity basketId;
    @Column(name = "sale_time")
    private Timestamp saleTime;
    @Column(name = "price")
    private Double price;

    public OrderEntity() {
    }

    public OrderEntity(UserEntity userId, BasketEntity basketId, Timestamp saleTime, double price) {
        this.userId = userId;
        this.basketId = basketId;
        this.saleTime = saleTime;
        this.price = price;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", userId=" + userId + ", basketId=" + basketId + ", saleTime=" + saleTime + ", price=" + price + '}';
    }
}
