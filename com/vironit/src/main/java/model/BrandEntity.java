package model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "brands", schema = "public")
public class BrandEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "value")
    private String value;

    public BrandEntity() {
    }

    public BrandEntity(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrandEntity that = (BrandEntity) o;

        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "{" + "id=" + id + ", value='" + value + '\'' + '}';
    }
}
