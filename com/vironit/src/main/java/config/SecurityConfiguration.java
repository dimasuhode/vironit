package config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import service.UserServiceImpl;



@Configuration
@EnableWebSecurity()
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
     @Autowired
    private UserServiceImpl userDetailsService;



    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        return authProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().
                 antMatchers("/adminlist","/delete","/updatedelete.jsp","/alluser").hasRole("ADMIN")

                .antMatchers("/").permitAll()
                .antMatchers("/index.jsp").permitAll()
                .antMatchers("/basket")
                .permitAll()
                .antMatchers("/registration")
                .permitAll()
                .antMatchers("/logout")
                .permitAll()
                .antMatchers("/main")
                .permitAll()

                .and().csrf().disable().formLogin().loginProcessingUrl("/entry").usernameParameter("login").passwordParameter("password").loginPage("/entry.jsp").defaultSuccessUrl("/").permitAll()
                .and()
                .logout()
                .permitAll();
//                .and().authorizeRequests()
//        .and().securityContext().configure(http);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**");
    }


}
