package dao;

import model.BrandEntity;
import util.SessionFactoryUtil;

import java.util.List;

public class BrandDaoImpl extends GenericsDao<Long, BrandEntity> {
    public BrandDaoImpl() {
        type = BrandEntity.class;
    }

    public BrandEntity getByValue(String value){
        List<BrandEntity> brandEntities =  SessionFactoryUtil.getSessionFactory().openSession().createQuery("From  model.BrandEntity  where value ='"+value+"'").list();
        return brandEntities.get(0);
    }
}
