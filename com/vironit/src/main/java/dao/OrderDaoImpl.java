package dao;

import lombok.extern.log4j.Log4j;
import model.OrderEntity;


@Log4j
public class OrderDaoImpl extends GenericsDao<Long, OrderEntity> {
    public OrderDaoImpl() {
        type = OrderEntity.class;
    }
}
