package dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import util.SessionFactoryUtil;

import java.io.Serializable;
import java.util.List;

public abstract class GenericsDao<L extends Serializable, T> {
    protected Class type;

    public List<T> getAll() {
        List<T> users = (List<T>) SessionFactoryUtil.getSessionFactory().openSession().createQuery("From " + type.getName()).list();
        return users;

    }

    public T getById(L id) {
        return (T) SessionFactoryUtil.getSessionFactory().openSession().get(type, id);

    }

    public boolean save(T entity) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.merge(entity);
        tx1.commit();
        session.close();
        return true;
    }

    public boolean delete(T entity) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(entity);
        tx1.commit();
        session.close();
        return true;
    }

    public boolean update(T entity) {
        Session session = SessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(entity);
        tx1.commit();
        session.close();
        return true;
    }
}
