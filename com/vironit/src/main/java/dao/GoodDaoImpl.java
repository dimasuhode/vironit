package dao;

import database.ConnectionDatabase;
import lombok.extern.log4j.Log4j;
import model.GoodEntity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Log4j
public class GoodDaoImpl extends GenericsDao<Long, GoodEntity> {
    public GoodDaoImpl() {
        type = GoodEntity.class;
    }
    @Override
    public boolean delete(GoodEntity entity) {
        String sql = "delete from goods where id=" + entity.getId();
        String sqlDeletekey = "delete from basket_goods where  good_id=" + entity.getId();
        try (Statement statement = ConnectionDatabase.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);) {
            statement.executeUpdate(sqlDeletekey);
            statement.executeUpdate(sql);
            return true;
        } catch (SQLException e) {
            log.warn("Can not be delete " + entity + "\n " + e.getMessage());
            return false;
        }
    }
}
