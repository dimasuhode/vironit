package dao;

import lombok.extern.log4j.Log4j;
import model.BasketEntity;


@Log4j
public class BasketDaoImpl extends GenericsDao<Long, BasketEntity> {

    public BasketDaoImpl() {
        type = BasketEntity.class;
    }

}
