package dao;

import model.CategoriesEntity;
import util.SessionFactoryUtil;

import java.util.List;

public class CategoriesDaoImpl extends GenericsDao<Long, CategoriesEntity> {
    public CategoriesDaoImpl() {
        type = CategoriesEntity.class;
    }

    public CategoriesEntity getByValue(String value) {
        List<CategoriesEntity> categoriesEntities = SessionFactoryUtil.getSessionFactory().openSession().createQuery("From  model.CategoriesEntity  where value ='" + value + "'").list();
        return categoriesEntities.get(0);
    }
}
