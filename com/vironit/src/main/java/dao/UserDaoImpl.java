package dao;

import lombok.extern.log4j.Log4j;
import model.UserEntity;
import util.SessionFactoryUtil;

import java.util.List;

@Log4j
public class UserDaoImpl extends GenericsDao<Long, UserEntity> {
    public UserDaoImpl() {
        type = UserEntity.class;
    }

    public UserEntity getByLogin(String login) {
        List<UserEntity> userEntityList =  SessionFactoryUtil.getSessionFactory().openSession().createQuery("From  model.UserEntity  where login ='"+login+"'").list();
        return userEntityList.get(0);
    }
}