package dao;

import model.RoleEntity;
import util.SessionFactoryUtil;

import java.util.List;

public class RoleDaoImpl extends GenericsDao<Long, RoleEntity> {
    public RoleDaoImpl() {
        type=RoleEntity.class;
    }
    public RoleEntity getRoleUser(){
        List<RoleEntity> roleEntities =  SessionFactoryUtil.getSessionFactory().openSession().createQuery("From  model.RoleEntity  where role_name ='ROLE_USER'").list();
        return roleEntities.get(0);
    }
}
