package service;

import dao.BrandDaoImpl;
import model.BrandEntity;
import org.springframework.stereotype.Service;

@Service
public class BrandService {
    private BrandDaoImpl brandDao = new BrandDaoImpl();

    public BrandEntity getByValue(String value) {
        return brandDao.getByValue(value);
    }
}
