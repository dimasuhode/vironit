package service;

import dao.UserDaoImpl;
import model.UserEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserDetailsService {
    private UserDaoImpl userDao = new UserDaoImpl();


    @Override
    public UserDetails loadUserByUsername(String login) {
        UserDaoImpl userDao = new UserDaoImpl();
        UserEntity userEntity = userDao.getByLogin(login);
        if (userEntity == null) {
            throw new UsernameNotFoundException(login);
        }
        return new UserPrincipal(userEntity);
    }
}
