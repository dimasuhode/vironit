package service;

import dao.CategoriesDaoImpl;
import model.CategoriesEntity;
import org.springframework.stereotype.Service;

@Service
public class CategoriesService {
    private CategoriesDaoImpl categoriesDao = new CategoriesDaoImpl();

    public CategoriesEntity getByValue(String value) {
        return categoriesDao.getByValue(value);
    }
}
