package service;

import dao.BasketDaoImpl;
import model.BasketEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BasketService {
    private BasketDaoImpl basketDao = new BasketDaoImpl();

    public List<BasketEntity> getAll() {
        return basketDao.getAll();
    }

    public BasketEntity getById(Long id) {
        return basketDao.getById(id);
    }

    public boolean save(BasketEntity entity) {
        return basketDao.save(entity);
    }

    public boolean delete(BasketEntity entity) {
        return basketDao.delete(entity);
    }

    public boolean update(BasketEntity entity) {
        return basketDao.update(entity);
    }
}
