package service;

import dao.OrderDaoImpl;
import model.OrderEntity;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderService {
    private OrderDaoImpl orderDao = new OrderDaoImpl();

    public OrderEntity getById(Long id) {
        return orderDao.getById(id);
    }

    public List<OrderEntity> getAll() {
        return orderDao.getAll();
    }

    public boolean save(OrderEntity entity) {
        return orderDao.save(entity);
    }

    public boolean delete(OrderEntity entity) {
        return orderDao.delete(entity);
    }

    public boolean update(OrderEntity entity) {
        return orderDao.update(entity);
    }
}
