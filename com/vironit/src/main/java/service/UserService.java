package service;

import dao.UserDaoImpl;
import model.UserEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private UserDaoImpl userDao = new UserDaoImpl();

    public UserEntity getById(Long id) {
        return userDao.getById(id);
    }

    public List<UserEntity> getAll(){
        return userDao.getAll();
    }

    public UserEntity getByLogin(String login) {
        return userDao.getByLogin(login);
    }

    public boolean save(UserEntity entity) {
        return userDao.save(entity);
    }

    public boolean delete(UserEntity entity) {
        return userDao.delete(entity);
    }

    public boolean update(UserEntity entity) {
        return userDao.update(entity);
    }
}
