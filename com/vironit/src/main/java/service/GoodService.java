package service;

import dao.GoodDaoImpl;
import model.GoodEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodService {
    private GoodDaoImpl goodDao = new GoodDaoImpl();

    public List<GoodEntity> getAll() {
        return goodDao.getAll();
    }

    public GoodEntity getById(Long id) {
        return goodDao.getById(id);
    }

    public boolean save(GoodEntity entity) {
        return goodDao.save(entity);
    }

    public boolean delete(GoodEntity entity) {
        return goodDao.delete(entity);
    }

    public boolean update(GoodEntity entity) {
        return goodDao.update(entity);
    }
}
