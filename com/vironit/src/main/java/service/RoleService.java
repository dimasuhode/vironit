package service;

import dao.RoleDaoImpl;
import model.RoleEntity;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private RoleDaoImpl roleDao = new RoleDaoImpl();

    public RoleEntity getRoleUser() {
        return roleDao.getRoleUser();
    }


}
