package service;

import model.GoodEntity;
import org.junit.Assert;
import org.junit.Test;

public class TestGoodService {

    private  GoodService goodService = new GoodService();
    @Test
    public void getAllTest() {
        Assert.assertNotNull(goodService.getAll());
    }
    @Test
    public void getGoodById(){
       GoodEntity goodEntity = goodService.getAll().get(0);
       GoodEntity testEntity = goodService.getById(goodEntity.getId());
       Assert.assertEquals(goodEntity,testEntity);
    }

}
