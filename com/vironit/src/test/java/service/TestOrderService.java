package service;

import model.OrderEntity;
import org.junit.Assert;
import org.junit.Test;

public class TestOrderService {
    private OrderService orderService = new OrderService();
    private OrderEntity orderEntity = orderService.getAll().get(0);

    @Test
    public void getAllTest(){
        Assert.assertNotNull(orderService.getAll());
    }

    @Test
    public void getByIdTest(){
        OrderEntity entityTest = orderService.getById(orderEntity.getId());
        Assert.assertNotNull(entityTest);
    }
}
