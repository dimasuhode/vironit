package service;

import model.BasketEntity;
import org.junit.Assert;
import org.junit.Test;

public class TestBasketService {
    private BasketService basketService = new BasketService();
    @Test
    public void getAllTest(){
        Assert.assertNotNull(basketService.getAll());
    }
    @Test
    public void getByIdTest(){
        BasketEntity basketEntity = basketService.getAll().get(0);
        BasketEntity entityTest = basketService.getById(basketEntity.getId());
        Assert.assertNotNull(entityTest);
    }
}
