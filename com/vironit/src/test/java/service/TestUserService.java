package service;

import model.UserEntity;
import org.junit.Assert;
import org.junit.Test;

public class TestUserService {
    private UserService userService = new UserService();
    private UserEntity userEntity = userService.getAll().get(0);
    @Test
    public void getAll(){
        Assert.assertNotNull(userService.getAll());
    }
    @Test
    public void getByIdTest(){
        UserEntity entityTest = userService.getById(userEntity.getId());
        Assert.assertEquals(userEntity,entityTest);
    }
    @Test
    public void getByLoginTest(){
        UserEntity entityTest = userService.getByLogin(userEntity.getLogin());
        Assert.assertEquals(userEntity,entityTest);
    }
}
