package mock;

import model.*;

import java.util.ArrayList;

public interface MockInterface {
    default UserEntity getUser(){
        return new UserEntity("DimaSyx","dima@mail.ru","qwerty",true,new RoleEntity("User"));
    }
    default GoodEntity getGood(){
        return new GoodEntity(new BrandEntity("Apple"),"iPhone 8s",new CategoriesEntity("Phone"),20,400,"");
    }
//    default BasketEntity getBasket(){
//        return new BasketEntity((long)1, new ArrayList<>());
//    }
}
